Simplified Docker role for Ansible by Archesky
=========

An ansible role that install Docker on Debian and Redhad based Linux.

Requirements
------------

None.

Role Variables
--------------

Variables are stored in defaults/main.yml. You don't have to change anything except docker-packages(optional):

```
docker_packages:
  - "docker-ce"
  - "docker-ce-cli"
  - "containerd.io"
  - "docker-buildx-plugin"
```

Dependencies
------------

None.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - heffle.docker

License
-------

BSD
